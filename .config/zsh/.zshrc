#!/usr/bin/env zsh

if [ -d "$ZDOTDIR/include" ]; then
  setopt nullglob
  for f in "$ZDOTDIR"/include/[_]?*.zsh; do
    source "$f"
  done
  unset f
  unsetopt nullglob
fi

# p10k
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
case $TTY in
  /dev/tty[0-9]*)
    [[ ! -f "$ZDOTDIR/.p10k-ascii.zsh" ]] || source "$ZDOTDIR/.p10k-ascii.zsh"
    ;;
  *)
    [[ ! -f "$ZDOTDIR/.p10k.zsh" ]] || source "$ZDOTDIR/.p10k.zsh"
    ;;
esac

# history
export HISTSIZE=10000
export SAVEHIST=10000

# colors
eval $(dircolors "$ZDOTDIR/dircolors/dircolors")

# syntax highlighting
source "$ZDOTDIR/themes/catppuccin_mocha-zsh-syntax-highlighting.zsh"
source "$ZDOTDIR/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

if [ -d "$ZDOTDIR/include" ]; then
  setopt nullglob
  for f in "$ZDOTDIR"/include/[!_]?*.zsh; do
    source "$f"
  done
  unset f
  unsetopt nullglob
fi

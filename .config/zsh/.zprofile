#!/usr/bin/env zsh

if [ -f "$XDG_CONFIG_HOME/tty/ttymaps.kmap" ]; then
  sudo -n loadkeys "$XDG_CONFIG_HOME/tty/ttymaps.kmap" 2>/dev/null
fi

if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  startx "$XINITRC" -- "$XSERVERRC" >/dev/null 2>&1
fi

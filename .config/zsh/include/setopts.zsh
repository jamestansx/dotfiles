# history
setopt append_history
setopt extended_history
setopt hist_fcntl_lock
setopt hist_find_no_dups
setopt hist_ignore_all_dups
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_no_store
setopt hist_reduce_blanks
setopt hist_save_no_dups
setopt hist_verify
setopt inc_append_history	# `fc -RI` to import history

# cd
setopt auto_cd
setopt auto_pushd
setopt cdable_vars
setopt pushd_ignore_dups
unsetopt pushd_silent
setopt pushd_to_home

# completion
setopt always_to_end
setopt auto_list
setopt auto_menu
setopt auto_name_dirs
setopt auto_param_keys
setopt auto_param_slash
setopt auto_remove_slash
setopt complete_in_word
setopt list_rows_first
setopt list_packed
unsetopt menu_complete

# expansion & globbing
setopt brace_ccl
setopt extended_glob
setopt glob_dots
setopt unset

# I/O
setopt correct
unsetopt correct_all
unsetopt flow_control
setopt interactive_comments

# job control
setopt notify
setopt long_list_jobs

# prompting
setopt prompt_subst
setopt transient_rprompt

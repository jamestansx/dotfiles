# initialization
autoload -U compinit
_ZSH_CACHE="$XDG_CACHE_HOME/zsh"
compinit -d "$_ZSH_CACHE/zcompdump-$ZSH_VERSION"
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "$_ZSH_CACHE/zcompcache"

# completers
zstyle ':completion:*' completer _extensions _complete _match _approximate

# color completion
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

# approximate completion
zstyle ':completion:*:approximate:*'	max-errors 1 numeric
zstyle -e ':completion:*:approximate:*'	max-errors \
  'reply=($((($#PREFIX+$#SUFFIX)/3>7?7:($#PREFIX+$#SUFFIX)/3)) numeric)'

# smart matching
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

# don't complete backup files as executables
zstyle ':completion:*:complete:-command-::commands' ignored-patterns '(aptitude-*|*\~)'

# completion messages
zstyle ':completion:*'			verbose true
zstyle ':completion:*:-command-:*:'	verbose false
zstyle ':completion:*:options'		description 'yes'
zstyle ':completion:*:options'		auto-description '%d'
zstyle ':completion:*:messages'		format '%d'
zstyle ':completion:*:descriptions'	format '%F{green}completing %B%d%b%f'
zstyle ':completion:*:warnings'		format '%F{yellow}No matches for:%f %d'

# menu select
zstyle ':completion:*' menu select=5

# group matches
zstyle ':completion:*:matches'	group 'yes'
zstyle ':completion:*'		group-name ''

# ignore completion functions for commands you don't have:
zstyle ':completion::(^approximate*):*:functions' ignored-patterns '(_*|pre(cmd|exec))'

# correction
zstyle ':completion:*:correct:*' original true
zstyle ':completion:*:correct:*' insert-unambiguous true
zstyle ':completion:*:corrections' format '%F{red}%d (errors: %e)%f'

# search path for sudo completion
zstyle ':completion:*:sudo:*' command-path /usr/local/sbin \
					   /usr/local/bin  \
					   /usr/sbin       \
					   /usr/bin        \
					   /sbin           \
					   /bin            \
					   /usr/X11R6/bin

# man command
zstyle ':completion:*:manuals' seperate-sections true
zstyle ':completion:*:manuals.*' insert-sections true
zstyle ':completion:*:man:*' menu yes select=5

# offer indexes before parameters in subscripts
zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters

# processes
zstyle ':completion:*:processes-names'	command 'ps c -u ${USER} -o command | uniq'
zstyle ':completion:*:processes'	command 'ps -au$USER'
zstyle ':completion:*:processes'	force-list always
zstyle ':completion:*:processes'	menu yes select=5
zstyle ':completion:*:processes'	list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;31=0=01'

# expand aliases
zstyle ':completion:*' complete true
zstyle ':completion:alias-expension:*' completer _expand_alias

# directories
zstyle ':completion:*' complete-options true

bindkey -v
export KEYTIMEOUT=1

# history search
bindkey -M viins '^R' history-incremental-search-backward
bindkey -M vicmd '^R' history-incremental-search-backward

# edit command line
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey '^E' edit-command-line

# menu completion
zmodload zsh/complist
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M menuselect 'l' vi-forward-char

# history navigation
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey -M vicmd k up-line-or-beginning-search
bindkey -M vicmd j down-line-or-beginning-search

# alias expansion
zle -C alias-expension complete-word _generic
bindkey '^X^A' alias-expansion

typeset -gA key=(
  Up		"${terminfo[kcuu1]}"
  Down		"${terminfo[kcud1]}"
  Home		"${terminfo[khome]}"
  End		"${terminfo[kend]}"
  Insert	"${terminfo[kich1]}"
  Delete	"${terminfo[kdch1]}"
  PageUp	"${terminfo[kpp]}"
  PageDown	"${terminfo[knp]}"

  Shift-Tab	"${terminfo[kcbt]}"
)

bindkey -- "${key[Home]}"       beginning-of-line
bindkey -- "${key[End]}"        end-of-line
bindkey -- "${key[Insert]}"     overwrite-mode
bindkey -- "${key[Delete]}"     delete-char
bindkey -- "${key[Up]}"         up-line-or-beginning-search
bindkey -- "${key[Down]}"       down-line-or-beginning-search
bindkey -- "${key[PageUp]}"     beginning-of-buffer-or-history
bindkey -- "${key[PageDown]}"   end-of-buffer-or-history
bindkey -- "${key[Shift-Tab]}"  reverse-menu-complete

if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
	autoload -Uz add-zle-hook-widget
	function zle_application_mode_start { echoti smkx }
	function zle_application_mode_stop { echoti rmkx }
	add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
	add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

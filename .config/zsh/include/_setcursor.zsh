zle-keymap-select() {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main  ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = ''     ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}

zle-line-init() {
  zle -K viins
}

zle -N zle-keymap-select
zle -N zle-line-init

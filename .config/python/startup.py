import atexit
import os
import readline

if "XDG_DATA_HOME" in os.environ:
    histdir = os.path.expanduser(os.environ["XDG_STATE_HOME"])
    histfile = os.path.join(histdir, "python", "pyhistfile")
elif "PYTHONHISTFILE" in environ:
    histfile = os.environ["PYTHONHISTFILE"]
else:
    histdir = os.path.join(os.path.expanduser("~"), ".local/state/python")
    histfile = os.path.join(histdir, "pyhistfile")

histfile = os.path.abspath(histfile)
_dir, _ = os.path.split(histfile)
os.makedirs(_dir, exist_ok=True)

try:
    readline.read_history_file(histfile)
    # default history len is -1 (infinite), which may grow unruly
    readline.set_history_length(1000)
except FileNotFoundError:
    pass

atexit.register(readline.write_history_file, histfile)
